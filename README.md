# flex-trab1
Primeiro trabalho _2019/1_ - **Compiladores**

# Instalação Flex
```
./install-flex.sh
```

# Clonar e executar
```
git clone https://gitlab.com/moreiraandre/flex-trab1.git
cd flex-trab1
make
```

# Referências
https://youtu.be/2umaGOKiv5k