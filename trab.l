DIGITO [0-9]
LETRA [a-zA-Z]

%%

"."                                 {printf("TOKEN: ponto");}
","                                 {printf("TOKEN: virgula");}
";"                                 {printf("TOKEN: ponto_virgula");}
":"                                 {printf("TOKEN: dois_pontos");}
"["                                 {printf("TOKEN: abre_col");}
"]"                                 {printf("TOKEN: fecha_col");}
"("                                 {printf("TOKEN: abre_par");}
")"                                 {printf("TOKEN: fecha_par");}
"'"                                 {printf("TOKEN: aspas");}

{DIGITO}+                           {printf("TOKEN num_inteiro");}
{DIGITO}+","{DIGITO}+               {printf("TOKEN num_real");}
{LETRA}                             {printf("TOKEN const_lit");}
{LETRA}({LETRA}|{DIGITO})31         {printf("TOKEN identificador");}

"*"                                 {printf("TOKEN: op_arit_mult");}
"/"                                 {printf("TOKEN: op_arit_div");}
"+"                                 {printf("TOKEN: op_arit_adi");}
"-"                                 {printf("TOKEN: op_ari_sub");}
"="                                 {printf("TOKEN: op_atrib");}
"=="                                {printf("TOKEN: op_rel_igual");}
"!="                                {printf("TOKEN: op_rel_naoigual");}
">"                                 {printf("TOKEN: op_rel_maior");}
">="                                {printf("TOKEN: op_rel_maiorigual");}
"<"                                 {printf("TOKEN: op_rel_menor");}
"<="                                {printf("TOKEN: op_rel_menorigual");}
"!"                                 {printf("TOKEN: op_log_nao");}
"&&"                                {printf("TOKEN: op_log_and");}
"||"                                {printf("TOKEN: op_log_or");}


algoritmo                           {printf("TOKEN: pr_algoritmo");}
Inicio                              {printf("TOKEN: pr_inicio");}
fim_algoritmo                       {printf("TOKEN: pr_fim_algo");}
LOGICO                              {printf("TOKEN: pr_logico");}
INTEIRO                             {printf("TOKEN: pr_inteiro");}
REAL                                {printf("TOKEN: pr_real");}
CARACTER                            {printf("TOKEN: pr_caracter");}
REGISTRO                            {printf("TOKEN: pr_registro");}
leia                                {printf("TOKEN: pr_leia");}
escreva                             {printf("TOKEN: pr_escreva");}
se                                  {printf("TOKEN: pr_se");}
entao                               {printf("TOKEN: pr_entao");}
senao                               {printf("TOKEN: pr_senao");}
fim_se                              {printf("TOKEN: pr_fim_se");}
para                                {printf("TOKEN: pr_para");}
ate                                 {printf("TOKEN: pr_ate");}
passo                               {printf("TOKEN: pr_passo");}
faça                                {printf("TOKEN: pr_faça");}
fim_para                            {printf("TOKEN: pr_fim_para");}
enquanto                            {printf("TOKEN: pr_enqto");}
fim_enquanto                        {printf("TOKEN: pr_fim_enqto");}
repita                              {printf("TOKEN: pr_repita");}
ABS                                 {printf("TOKEN: pr_abs");}
TRUNCA                              {printf("TOKEN: pr_trunca");}
RESTO                               {printf("TOKEN: pr_resto");}
declare                             {printf("TOKEN: pr_declare");}
%%

int main (void) {
    yylex();
    return 0;
}

int yywrap() {
    return 1;
}
